package element.controller;


import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/premium")
public class PremiumCalculatorController {
	
	@GetMapping(path="/hello/{input}", produces=MediaType.APPLICATION_JSON_VALUE)
	public String hello(@PathVariable String input)
	{
		return "hello " + input;
	}
}
