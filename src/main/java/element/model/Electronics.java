package element.model;

public class Electronics implements InsurableProduct {
	@Override
	public final int getMinimumCoverage() {
		return 500;
	}

	@Override
	public final int getMaximumCoverage() {
		return 6000;
	}

	@Override
	public final float getPercentageRisk() {
		return 35 / 100f;
	}
}
