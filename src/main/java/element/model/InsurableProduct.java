package element.model;

public interface InsurableProduct {
	int getMinimumCoverage();
    int getMaximumCoverage();
    float getPercentageRisk();
    
  //  boolean canBeInsuredFor(User user);
}
