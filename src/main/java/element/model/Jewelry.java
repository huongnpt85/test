package element.model;

public class Jewelry implements InsurableProduct {
	@Override
	public final int getMinimumCoverage() {
		return 500;
	}

	@Override
	public final int getMaximumCoverage() {
		return 10000;
	}

	@Override
	public final float getPercentageRisk() {
		return 5 / 100f;
	}
}
