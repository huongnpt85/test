package element.model;

public class SportsEquipment implements InsurableProduct {
	@Override
	public final int getMinimumCoverage() {
		return 0;
	}

	@Override
	public final int getMaximumCoverage() {
		return 20000;
	}

	@Override
	public final float getPercentageRisk() {
		return 30 / 100f;
	}
}
