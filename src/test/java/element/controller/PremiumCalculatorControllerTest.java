package element.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import element.controller.PremiumCalculatorController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PremiumCalculatorControllerTest {
	private MockMvc mockMvc;

	@Autowired
	private PremiumCalculatorController controller;

	@Before
	public void setup() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(this.controller).build();
	}

	@Test
	public void testHello() throws Exception {
		MvcResult mvcResult = this.mockMvc
			    .perform(get("/premium/hello/Anna")
			               .contentType(APPLICATION_JSON))
			               .andExpect(status().isOk())
			               .andReturn();
		String responseAsJson = "hello Anna"; 
		assertEquals("response does not match", mvcResult.getResponse().getContentAsString(),
			    responseAsJson);


	}
	
	@Test
	public void testHelloWithoutMvcResult() throws Exception {
		this.mockMvc
			    .perform(get("/premium/hello/Ann1")
			               .contentType(APPLICATION_JSON))
			               .andExpect(status().isOk())
			               .andExpect(jsonPath("$", is("hello Anna")));
	}
}
